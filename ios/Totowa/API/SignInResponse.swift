//
// Created by Brian Romanko on 7/6/18.
// Copyright (c) 2018 bromanko llc. All rights reserved.
//

import Foundation

class SignInResponse: ResponseObjectSerializable {
    let accessToken: String
    let expiresAt: Date

    required init?(response: HTTPURLResponse, representation: Any) {
        guard
            let representation = representation as? [String: Any],
            let accessToken = representation["accessToken"] as? String,
            let expiresAt = representation["expiresAt"] as? Date
        else { return nil }

        self.accessToken = accessToken
        self.expiresAt = expiresAt
    }

    init(accessToken: String, expiresAt: Date) {
        self.accessToken = accessToken
        self.expiresAt = expiresAt
    }
}
