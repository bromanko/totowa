//
// Created by Brian Romanko on 7/13/18.
// Copyright (c) 2018 bromanko llc. All rights reserved.
//

import Foundation
import Alamofire

extension Request {
    public func debugLog() -> Self {
#if DEBUG
        debugPrint(self)
#endif
        return self
    }
}
