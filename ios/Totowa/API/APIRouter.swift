import Foundation
import ReSwift
import Alamofire

enum APIRouter {
    // swiftlint:disable all
    case signIn(idToken: String)
    // swiftlint:enable all

    var method: HTTPMethod {
        switch self {
        case .signIn:
            return .post
        }
    }

    var path: String {
        switch self {
        case .signIn:
            return "/signin"
        }
    }

    var parameters: Parameters? {
        switch self {
        case .signIn(let idToken):
            return ["idToken": idToken]
        }
    }
}

extension APIRouter: URLRequestConvertible {
    func asURLRequest() throws -> URLRequest {
        let baseURLString = container.resolve(Store<AppState>.self)!.state.api.baseURL
        let baseURL = try baseURLString.asURL()

        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue

        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)

        if let parameters = parameters {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }

        return urlRequest
    }
}
