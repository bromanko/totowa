import Foundation

// Workaround until new SwiftLint release
// https://github.com/realm/SwiftLint/issues/2231
// swiftlint:disable all
enum APIError: Error {
    case network(error: Error)
    case jsonSerialization(error: Error)
    case objectSerialization(reason: String)
}
// swiftlint:enable all
