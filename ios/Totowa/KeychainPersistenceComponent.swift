import Foundation
import ReSwift
import SwiftKeychainWrapper

enum KeychainError: Error {
    case nilStore
}

private let accessTokenKey = "AccessToken"

class KeychainPersistenceComponent {
    func saveState(state: AccessTokenState?) {
        let jsonData = try? JSONEncoder().encode(state)
        if jsonData == nil {
            KeychainWrapper.standard.removeObject(forKey: accessTokenKey)
        } else {
            KeychainWrapper.standard.set(jsonData!, forKey: accessTokenKey)
        }
    }

    func loadState() -> AccessTokenState? {
        var state: AccessTokenState?
        let loaded = KeychainWrapper.standard.data(forKey: accessTokenKey)
        if loaded != nil {
            state = try? JSONDecoder().decode(AccessTokenState.self, from: loaded!)
        }
        return state
    }
}
