import UIKit
import MapKit

class LocationsViewController: UIViewController {
    var zoomedToUser = false

    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var locationButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        LocationComponent().startMonitoringSignificantLocations()
    }

    @IBAction func navigateToCurrentLocation(_ sender: UIButton) {
        map.showAnnotations([map.userLocation], animated: true)
    }
}

extension LocationsViewController: MKMapViewDelegate {

    func mapView(_ mapView: MapKit.MKMapView, didUpdate userLocation: MapKit.MKUserLocation) {
        guard zoomedToUser else {
            mapView.showAnnotations([userLocation], animated: true)
            zoomedToUser = true
            return
        }
    }
}
