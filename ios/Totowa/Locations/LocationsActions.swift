import Foundation
import ReSwift

struct AddLocation: Action {
    let location: Location
    let createdAt: Date

    init (_ location: Location) {
        self.location = location
        self.createdAt = Date()
    }
}
