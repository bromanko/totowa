import Foundation
import ReSwift
import MapKit

struct Location: StateType, Codable {
    var createdAt: Date
    var latitude: CLLocationDegrees
    var longitude: CLLocationDegrees
}

struct LocationsState: StateType, Codable {
    var locations: [Location]
}
