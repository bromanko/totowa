import Foundation
import INTULocationManager

class LocationComponent {
    func startMonitoringSignificantLocations() {
        INTULocationManager.sharedInstance().subscribeToSignificantLocationChanges({
            currentLocation, achievedAccuracy, status in
            if status == INTULocationStatus.success {
                print("Updating location")
            } else {
                print("An error occurred getting location data.")
            }
        })
    }
}
