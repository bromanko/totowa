import UIKit
import Foundation

let loadingRoute: RouteElementIdentifier = "Loading"
let signInRoute: RouteElementIdentifier = "SignIn"
let homeRoute: RouteElementIdentifier = "Home"
let locationsRoute: RouteElementIdentifier = "Locations"
