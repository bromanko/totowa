import Foundation
import UIKit


class HomeViewController: UITabBarController {
    var contentLayerName = "content"

    override func viewDidLoad() {
        self.tabBar.layer.shadowColor = UIColor.black.cgColor
        self.tabBar.layer.shadowOffset = .zero
        self.tabBar.layer.shadowOpacity = 0.2
        self.tabBar.layer.shadowRadius = 4

        self.tabBar.layer.cornerRadius = 5
    }
}
