import Foundation
import ReSwift

struct AccessTokenState: StateType, Codable {
    var value: String = ""
    var expiresAt: Date = Date.distantPast

    init () {}

    init(_ value: String, _ expiresAt: Date) {
        self.value = value
        self.expiresAt = expiresAt
    }
}

struct AuthState: StateType, Codable {
    var googleClientId: String = "235429594659-rhccd4rbhgf9n0gu1fnbc3svvl713nu1.apps.googleusercontent.com"
    var accessToken: AccessTokenState = AccessTokenState()

    init () {}
}

extension AuthState {
    var isAuthenticated: Bool {
        return !(accessToken.value.isEmpty) &&
            Date().compare(accessToken.expiresAt) == ComparisonResult.orderedAscending
    }
}
