//
//  SignInViewController.swift
//  Totowa
//
//  Created by Brian Romanko on 6/11/18.
//  Copyright © 2018 bromanko llc. All rights reserved.
//

import UIKit
import GoogleSignIn

class SignInViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
    }
}

extension SignInViewController: GIDSignInUIDelegate {

}
