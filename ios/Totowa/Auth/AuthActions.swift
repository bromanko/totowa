import Foundation
import ReSwift

struct AccessTokenRetrievedAction: Action {
    let accessToken: AccessTokenState?
}

struct SignInFailedAction: Action {}
struct SignInSucceededAction: Action {}
