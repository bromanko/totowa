import Foundation
import GoogleSignIn
import ReSwift
import Alamofire

class AuthComponent: NSObject {
    private let store: Store<AppState>
    private let gidSignIn: GIDSignIn
    private let keychain: KeychainPersistenceComponent

    init(store: Store<AppState>, gidSignIn: GIDSignIn, keychainComponent: KeychainPersistenceComponent) {
        self.store = store
        self.gidSignIn = gidSignIn
        self.keychain = keychainComponent

        super.init()

        gidSignIn.clientID = store.state.auth.googleClientId
        gidSignIn.delegate = self
    }

    func signIn() {
        store.dispatch(AccessTokenRetrievedAction(accessToken: keychain.loadState()))

        if store.state.auth.isAuthenticated {
            let accessToken = store.state.auth.accessToken
            signInSucceededWithResponse(
                    SignInResponse(accessToken: accessToken.value, expiresAt: accessToken.expiresAt)
            )
        } else {
            gidSignIn.signInSilently()
        }
    }

    func handleSignInUrl(_ url: URL, _ options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
        return gidSignIn.handle(url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation])
    }

    private func signInSucceededWithResponse(_ response: SignInResponse) {
        let accessTokenState = AccessTokenState(response.accessToken, response.expiresAt)
        keychain.saveState(state: accessTokenState)

        store.dispatch(SignInSucceededAction())
        store.dispatch(SetRouteAction([homeRoute]))
    }

    private func signInFailedWithError(_ error: Error!) {
        print("\(error.debugDescription)")

        store.dispatch(SignInFailedAction())
        store.dispatch(SetRouteAction([signInRoute]))
    }
}

extension AuthComponent: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard error == nil else {
            signInFailedWithError(error)
            return
        }

        Alamofire
                .request(APIRouter.signIn(idToken: user.authentication.idToken))
                .validate()
                .debugLog()
                .responseObject(completionHandler: { (response: DataResponse<SignInResponse>) in
                    switch response.result {
                    case .success:
                        self.signInSucceededWithResponse(response.result.value!)
                    case .failure:
                        self.signInFailedWithError(response.error)
                    }
                }
        )
    }
}
