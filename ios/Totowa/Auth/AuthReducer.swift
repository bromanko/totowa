import Foundation
import ReSwift

func authReducer(action: Action, state: AuthState?) -> AuthState {
    var state = state ?? AuthState()

    switch action {
    case let action as AccessTokenRetrievedAction:
        state.accessToken = action.accessToken ?? AccessTokenState()
    default:
        break
    }

    return state
}
