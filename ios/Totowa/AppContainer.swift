import Foundation
import ReSwift
import Swinject
import SwinjectStoryboard
import GoogleSignIn

let container: Container = {
    let container = Container()

    // Store
    container
        .register(Store.self) { _ in
            Store(
                reducer: appReducer,
                state: nil,
                middleware: []
            )
        }
        .inObjectScope(.container)

    // Persistence
    container
        .register(KeychainPersistenceComponent.self) { _ in KeychainPersistenceComponent() }
        .inObjectScope(.container)

    // Auth
    container
        .register(GIDSignIn.self) { _ in GIDSignIn.sharedInstance() }
        .inObjectScope(.container)
    container
        .register(AuthComponent.self) { res in
            AuthComponent(
                store: res.resolve(Store.self)!,
                gidSignIn: res.resolve(GIDSignIn.self)!,
                keychainComponent: res.resolve(KeychainPersistenceComponent.self)!
            )
        }
        .inObjectScope(.container)

    // Navigation
    container
        .register(UIWindow.self) { _ in UIWindow(frame: UIScreen.main.bounds) }
        .inObjectScope(.container)
    container.register(UIStoryboard.self) { _ in
        SwinjectStoryboard.create(name: "Main", bundle: nil, container: container)
    }
    container.storyboardInitCompleted(LoadingViewController.self) { res, con in }
    container.storyboardInitCompleted(SignInViewController.self) { res, con in }
    container.storyboardInitCompleted(HomeViewController.self) { res, con in }
    container
        .register(NavigationComponent.self) { res in
            NavigationComponent(
                window: res.resolve(UIWindow.self)!,
                storyboard: res.resolve(UIStoryboard.self)!
            )
        }
        .inObjectScope(.container)

    // Locations
    container
        .register(LocationComponent.self) { _ in LocationComponent() }
        .inObjectScope(.container)

    return container
}()
