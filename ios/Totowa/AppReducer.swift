import Foundation
import ReSwift

func appReducer(action: Action, state: AppState?) -> AppState {
    return AppState(
        api: apiReducer(action: action, state: state?.api),
        auth: authReducer(action: action, state: state?.auth),
        navigation: navigationReducer(action: action, state: state?.navigation),
        locations: locationsReducer(action: action, state: state?.locations)
    )
}
