import Foundation
import ReSwift

func navigationReducer(action: Action, state: NavigationState?) -> NavigationState {
    var state = state ?? NavigationState()

    switch action {
    case let action as SetRouteAction:
        state.route = action.route
        state.changeRouteAnimated = action.animated
    default:
        break
    }

    return state
}
