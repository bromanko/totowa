import UIKit
import Foundation
import ReSwift

class NavigationComponent: NSObject, StoreSubscriber {
    private let window: UIWindow
    private let storyboard: UIStoryboard

    init(window: UIWindow, storyboard: UIStoryboard) {
        self.window = window
        self.storyboard = storyboard
    }

    func subscribeStore(_ store: Store<AppState>) {
        store.subscribe(self) { subscription in
            subscription.select {
                state in state.navigation
            }
        }
    }

    func newState(state: NavigationState) {
        guard !state.route.isEmpty else {
            print("Received an empty route. Ignoring.")
            return
        }

        let vcIdentifier = state.route[0]
        let viewController = storyboard.instantiateViewController(withIdentifier: vcIdentifier)

        let duration = state.changeRouteAnimated ? 0.3 : 0
        UIView.transition(with: window, duration: duration, options: .transitionCrossDissolve, animations: {
            self.window.rootViewController = viewController
        }, completion: nil)
    }
}
