import Foundation
import ReSwift

struct SetRouteAction: Action {
    let route: Route
    let animated: Bool

    public init(_ route: Route, animated: Bool = true) {
        self.route = route
        self.animated = animated
    }
}
