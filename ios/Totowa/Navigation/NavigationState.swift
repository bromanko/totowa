import Foundation
import ReSwift

public typealias RouteElementIdentifier = String
public typealias Route = [RouteElementIdentifier]

struct NavigationState: StateType, Codable {
    var route: Route
    var changeRouteAnimated: Bool

    public init() {
        self.init(route: [], changeRouteAnimated: true)
    }

    public init(route: Route = [], changeRouteAnimated: Bool = true) {
        self.route = route
        self.changeRouteAnimated = changeRouteAnimated
    }
}
