import Foundation
import ReSwift

struct AppState: StateType, Codable {
    var api: APIState
    var auth: AuthState
    var navigation: NavigationState
    var locations: LocationsState
}
