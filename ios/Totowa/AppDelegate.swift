import UIKit
import ReSwift
import SwinjectStoryboard

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var auth: AuthComponent = container.resolve(AuthComponent.self)!

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = container.resolve(UIWindow.self)
        window?.makeKeyAndVisible()
        window?.rootViewController = container.resolve(UIStoryboard.self)!.instantiateInitialViewController()

        if launchOptions?[UIApplication.LaunchOptionsKey.location] != nil {
            // Resume monitoring for significant locations
            container.resolve(LocationComponent.self)!.startMonitoringSignificantLocations()
        }

        let store = container.resolve(Store<AppState>.self)!
        container.resolve(NavigationComponent.self)!.subscribeStore(store)

//        auth.signIn()
        store.dispatch(SetRouteAction([homeRoute]))

        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
        return auth.handleSignInUrl(url, options)
    }

}
