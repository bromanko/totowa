package com.bromanko.totowa

import com.bromanko.totowa.core.auth.AccessTokenProvider
import io.dropwizard.testing.DropwizardTestSupport
import io.restassured.http.ContentType
import io.restassured.specification.RequestSpecification

fun RequestSpecification.When(): RequestSpecification {
    return this.`when`()
}

fun RequestSpecification.testServer(server: DropwizardTestSupport<TotowaConfiguration>): RequestSpecification {
    return this
        .port(server.localPort)
        .contentType(ContentType.JSON)
}

fun RequestSpecification.auth(accessTokenProvider: AccessTokenProvider, userId: String): RequestSpecification {
    val token = accessTokenProvider.getPerishableToken(userId)
    return this.auth().oauth2(token.accessToken)
}