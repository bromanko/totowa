package com.bromanko.totowa

import org.jetbrains.spek.api.dsl.SpecBody

fun SpecBody.withMongoDb(component: TestSingletonComponent, body: SpecBody.() -> Unit) {
    val embeddedMongoManager = component.embeddedMongoManager
    val mongoTemplate = component.mongoTemplate

    beforeGroup {
        embeddedMongoManager.start()
    }

    body()

    afterEachTest {
        val template = mongoTemplate.get()
        template.createClient().dropDatabase(template.databaseName)
    }

    afterGroup {
        embeddedMongoManager.stop()
    }
}
