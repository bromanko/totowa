package com.bromanko.totowa

import org.jetbrains.spek.api.dsl.SpecBody

fun SpecBody.withAxon(component: TestSingletonComponent, body: SpecBody.() -> Unit) {
    val axonManager = component.axonManager
    beforeGroup {
        axonManager.start()
    }

    body()

    afterGroup {
        axonManager.stop()
    }
}