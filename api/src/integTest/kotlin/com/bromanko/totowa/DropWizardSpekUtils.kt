package com.bromanko.totowa

import io.dropwizard.testing.ConfigOverride
import io.dropwizard.testing.DropwizardTestSupport
import org.jetbrains.spek.api.dsl.SpecBody

val SpecBody.dropwizard: DropwizardTestSupport<TotowaConfiguration>
    by lazy {
        DropwizardTestSupport<TotowaConfiguration>(
            TotowaApplication::class.java,
            "totowa.yaml",
            ConfigOverride.config(
                "server.connector.port",
                "0"
            ),
            ConfigOverride.config("useInMemoryDb", "true"),
            ConfigOverride.config("auth.secret", "secretlysecretlysecretlysecretlysecretlysecretlysecretlysecretly")
        )
    }

fun SpecBody.withDropwizard(body: SpecBody.() -> Unit) {
    beforeGroup {
        dropwizard.before()
    }

    body()

    afterGroup {
        dropwizard.after()
    }
}