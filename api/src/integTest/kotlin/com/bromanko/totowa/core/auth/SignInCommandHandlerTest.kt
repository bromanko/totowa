package com.bromanko.totowa.core.auth

import com.bromanko.totowa.buildTestSingletonComponent
import com.bromanko.totowa.core.domain.consumer.user.SignInCommand
import com.bromanko.totowa.shouldBeAfter
import com.bromanko.totowa.withAxon
import com.bromanko.totowa.withMongoDb
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.experimental.runBlocking
import org.amshove.kluent.`should not be`
import org.amshove.kluent.shouldNotBeEmpty
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.joda.time.LocalDateTime

class SignInCommandHandlerTest : Spek({
    val component = buildTestSingletonComponent()

    withMongoDb(component) {
        withAxon(component) {
            describe("SignInCommandHandler") {
                it("creates user and returns token") {
                    val mockVerifier = mockk<GoogleTokenVerifier>()
                    val payload = GoogleIdToken.Payload()
                    payload.subject = "google12345"
                    every { mockVerifier.verify(any()) }.returns(payload)

                    runBlocking {
                        val handler = SignInCommandHandler(
                            mockVerifier,
                            component.accessTokenProvider,
                            component.queryGateway,
                            component.commandGateway
                        )
                        val response = handler.handle(SignInCommand("ignored"))

                        response `should not be` null
                        response.accessToken.shouldNotBeEmpty()
                        response.expiresAt.shouldBeAfter(LocalDateTime())
                    }
                }
            }
        }
    }
})

