package com.bromanko.totowa.core.domain.consumer.user

import com.bromanko.totowa.buildTestSingletonComponent
import com.bromanko.totowa.withMongoDb
import org.amshove.kluent.shouldEqual
import org.amshove.kluent.shouldNotEqual
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

class UserSummaryProjectionTest : Spek({
    val component = buildTestSingletonComponent()

    withMongoDb(component) {
        describe("UserSummary Projection") {
            describe("FindUserQuery") {
                it("finds a user") {
                    val projection = UserSummaryProjection(component.mongoTemplate)
                    val queries = UserSummaryProjection(component.mongoTemplate)

                    val userId = UserId.create()
                    val googleUserId = "googleUserId"
                    projection.on(UserCreatedEvent(userId, googleUserId))

                    val result = queries.handle(FindUserQuery(userId.id))
                    result shouldNotEqual null
                    result!!.userId shouldEqual userId.id
                    result.googleUserId shouldEqual googleUserId
                }

                it("does not find a user") {
                    val queries = UserSummaryProjection(component.mongoTemplate)

                    val result = queries.handle(FindUserQuery(UserId.create().id))
                    result shouldEqual null
                }
            }

            describe("FindUserByGoogleUserIdQuery") {
                it("finds a user") {
                    val projection = UserSummaryProjection(component.mongoTemplate)
                    val queries = UserSummaryProjection(component.mongoTemplate)

                    val userId = UserId.create()
                    val googleUserId = "googleUserId"
                    projection.on(UserCreatedEvent(userId, googleUserId))

                    val result = queries.handle(FindUserByGoogleUserIdQuery(googleUserId))
                    result shouldNotEqual null
                    result!!.googleUserId shouldEqual googleUserId
                    result.userId shouldEqual userId.id
                }

                it("does not find a user") {
                    val queries = UserSummaryProjection(component.mongoTemplate)

                    val result = queries.handle(FindUserByGoogleUserIdQuery("nope"))
                    result shouldEqual null
                }
            }
        }
    }
})