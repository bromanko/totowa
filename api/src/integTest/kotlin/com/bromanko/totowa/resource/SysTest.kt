package com.bromanko.totowa.resource

import com.bromanko.totowa.When
import com.bromanko.totowa.dropwizard
import com.bromanko.totowa.withDropwizard
import io.restassured.RestAssured.given
import org.hamcrest.CoreMatchers
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

class SysTest : Spek({
    withDropwizard {
        describe("Admin resource") {
            describe("ping") {
                describe("GET") {
                    it("returns data") {
                        given()
                            .port(dropwizard.adminPort)
                            .When()
                            .get("/sys/ping")
                            .then()
                            .statusCode(200)
                            .body(CoreMatchers.containsString("pong"))
                    }
                }
            }
        }
    }
})
