package com.bromanko.totowa.resource

import com.bromanko.totowa.When
import com.bromanko.totowa.auth
import com.bromanko.totowa.buildTestSingletonComponent
import com.bromanko.totowa.dropwizard
import com.bromanko.totowa.testServer
import com.bromanko.totowa.withDropwizard
import io.restassured.RestAssured
import org.hamcrest.core.IsEqual
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

class LocationsTest : Spek({
    val component = buildTestSingletonComponent()

    withDropwizard {
        describe("Locations resource") {
            describe("POST") {
                it("requires authentication") {
                    RestAssured.given()
                        .testServer(dropwizard)
                        .When()
                        .post("/locations")
                        .then()
                        .statusCode(401)
                }

                it("requires a request body") {
                    RestAssured.given()
                        .testServer(dropwizard)
                        .auth(component.accessTokenProvider, "userId")
                        .When()
                        .post("/locations")
                        .then()
                        .statusCode(422)
                }

                it("requires a valid request body") {
                    RestAssured.given()
                        .testServer(dropwizard)
                        .auth(component.accessTokenProvider, "userId")
                        .body(object {
                            val foo = "bar"
                        })
                        .When()
                        .post("/locations")
                        .then()
                        .statusCode(400)
                        .body("message", IsEqual.equalTo("Unable to process JSON"))
                }

                it("Creates the location") {

                }
            }
        }
    }
})