package com.bromanko.totowa.resource

import com.bromanko.totowa.dropwizard
import com.bromanko.totowa.withDropwizard
import io.restassured.RestAssured.given
import org.hamcrest.core.IsEqual.equalTo
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

class HomeTest : Spek({
    withDropwizard {
        describe("Home resource") {
            describe("GET") {
                it("returns data") {
                    given()
                        .port(dropwizard.localPort)
                        .get("/").then()
                        .statusCode(200)
                        .body("name", equalTo("Totowa"))
                }
            }
        }
    }
})
