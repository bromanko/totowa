package com.bromanko.totowa.resource

import com.bromanko.totowa.When
import com.bromanko.totowa.dropwizard
import com.bromanko.totowa.testServer
import com.bromanko.totowa.withDropwizard
import io.restassured.RestAssured
import org.hamcrest.core.IsEqual
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

class SignInTest : Spek({
    withDropwizard {
        describe("SignIn resource") {
            describe("POST") {
                it("requires a request body") {
                    RestAssured.given()
                        .testServer(dropwizard)
                        .When()
                        .post("/signin")
                        .then()
                        .statusCode(422)
                }

                it("requires a valid request body") {
                    RestAssured.given()
                        .testServer(dropwizard)
                        .body(object {
                            val foo = "bar"
                        })
                        .When()
                        .post("/signin")
                        .then()
                        .statusCode(400)
                        .body("message", IsEqual.equalTo("Unable to process JSON"))
                }

                it("requires a valid token") {
                    RestAssured.given()
                        .testServer(dropwizard)
                        .body(object {
                            val idToken = "bar"
                        })
                        .When()
                        .post("/signin")
                        .then()
                        .statusCode(400)
                        .body("message", IsEqual.equalTo("idToken is invalid"))
                }
            }
        }
    }
})