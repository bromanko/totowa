package com.bromanko.totowa

import com.bromanko.totowa.config.AuthConfiguration
import com.bromanko.totowa.config.EventStoreConfiguration
import com.bromanko.totowa.config.GoogleConfiguration
import com.bromanko.totowa.config.MongoDbConfiguration
import com.bromanko.totowa.config.ViewStoreConfiguration

private const val TEST_DATABASE_NAME = "totowa-test"

fun buildTestConfiguration() =
    TotowaConfiguration(
        useInMemoryDb = true,
        eventStore = EventStoreConfiguration(MongoDbConfiguration("", TEST_DATABASE_NAME)),
        viewStore = ViewStoreConfiguration(MongoDbConfiguration("", TEST_DATABASE_NAME)),
        auth = AuthConfiguration(secret = "secretlysecretlysecretlysecretlysecretlysecretlysecretlysecretly"),
        google = GoogleConfiguration("clientid")
    )