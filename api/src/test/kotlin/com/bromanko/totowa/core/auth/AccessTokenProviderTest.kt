package com.bromanko.totowa.core.auth

import com.bromanko.totowa.config.AuthConfiguration
import com.bromanko.totowa.shouldBeAfter
import com.bromanko.totowa.shouldBeBefore
import org.amshove.kluent.shouldNotBeEmpty
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.joda.time.LocalDateTime

class AccessTokenProviderTest : Spek({
    val secret = "secretlysecretlysecretlysecretlysecretlysecretlysecretlysecretly"

    describe("AccessTokenProvider") {
        describe("getPerishableToken") {
            it("retrieves a token") {
                val authConfig = AuthConfiguration(expirationTimeSeconds = 60 * 10, secret = secret)
                val provider = AccessTokenProvider(authConfig)

                val token = provider.getPerishableToken("userId")

                token.accessToken.shouldNotBeEmpty()
                token.expiresAt shouldBeAfter LocalDateTime.now()
                token.expiresAt shouldBeBefore LocalDateTime.now().plusMinutes(15)
            }
        }
    }
})