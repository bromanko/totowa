package com.bromanko.totowa.core.auth

import org.amshove.kluent.shouldBe
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jose4j.jwt.JwtClaims
import org.jose4j.jwt.consumer.JwtContext

class JwtAuthenticatorTest : Spek({
    describe("Jwt Authenticator") {
        it("creates UserPrincipal with valid subject") {
            val authenticator = JwtAuthenticator()
            val claims = JwtClaims()
            claims.subject = "foo"
            val context = JwtContext(claims, mutableListOf())

            val principal = authenticator.authenticate(context)

            principal.isPresent shouldBe true
            principal.get().name shouldBe "foo"
        }

        it("returns nothing with invalid subject") {
            val authenticator = JwtAuthenticator()
            val claims = JwtClaims()
            val context = JwtContext(claims, mutableListOf())

            val principal = authenticator.authenticate(context)

            principal.isPresent shouldBe false
        }
    }
})