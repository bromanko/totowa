package com.bromanko.totowa.core.domain.consumer.user

import org.axonframework.test.aggregate.AggregateTestFixture
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

class UserTest : Spek({
    describe("User Aggregate") {
        lateinit var fixture: AggregateTestFixture<User>

        beforeEachTest {
            fixture = AggregateTestFixture(User::class.java)
        }

        it("should create user") {
            val id = UserId.create()
            val googleUserId = "googleUserId"
            fixture
                .given()
                .`when`(CreateUserCommand(id, googleUserId))
                .expectSuccessfulHandlerExecution()
                .expectEvents(UserCreatedEvent(id, googleUserId))
        }

        describe("Move Device") {
            it("should emit correct events") {
                val userId = UserId.create()
                val device = Device("deviceId")
                val location = Location(0, 0, 0)

                fixture
                    .given(UserCreatedEvent(userId, "googleUserId"))
                    .`when`(MoveDeviceCommand(userId, device, location))
                    .expectSuccessfulHandlerExecution()
                    .expectEvents(
                        UserDeviceAddedEvent(userId, device),
                        UserDeviceMovedEvent(
                            userId,
                            device.deviceId,
                            location
                        )
                    )
            }
        }
    }
})