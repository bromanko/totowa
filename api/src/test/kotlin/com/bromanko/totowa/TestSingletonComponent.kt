package com.bromanko.totowa

import com.bromanko.totowa.config.TotowaConfigurationModule
import com.bromanko.totowa.core.VIEW_STORE
import com.bromanko.totowa.core.auth.AccessTokenProvider
import com.bromanko.totowa.core.axon.AxonModule
import com.bromanko.totowa.core.mongo.MongoModule
import com.bromanko.totowa.core.mongo.TotowaMongoTemplate
import com.bromanko.totowa.manager.AxonManager
import com.bromanko.totowa.manager.EmbeddedMongoManager
import dagger.BindsInstance
import dagger.Component
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.queryhandling.QueryGateway
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        TotowaConfigurationModule::class,
        DropwizardModule::class,
        MongoModule::class,
        AxonModule::class
    ]
)
interface TestSingletonComponent {
    val accessTokenProvider: AccessTokenProvider
    val queryGateway: QueryGateway
    val commandGateway: CommandGateway
    val axonManager: AxonManager
    val embeddedMongoManager: EmbeddedMongoManager
    @get:Named(VIEW_STORE)
    val mongoTemplate: dagger.Lazy<TotowaMongoTemplate>

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun configuration(configuration: TotowaConfiguration): Builder

        fun build(): TestSingletonComponent
    }
}

fun buildTestSingletonComponent(): TestSingletonComponent {
    return DaggerTestSingletonComponent
        .builder()
        .configuration(buildTestConfiguration())
        .build()
}