package com.bromanko.totowa

import org.joda.time.LocalDateTime
import org.joda.time.LocalTime
import org.junit.Assert.assertTrue

infix fun LocalDateTime.shouldBeAfter(expected: LocalDateTime) =
    assertTrue("Expected $this to be after $expected", this > expected)

infix fun LocalDateTime.shouldBeAfter(theTime: LocalTime) =
    assertTrue("Expected $this to be after $theTime", this.toLocalTime() > theTime)

infix fun LocalDateTime.shouldBeBefore(expected: LocalDateTime) =
    assertTrue("Expected $this to be before $expected", this < expected)

infix fun LocalDateTime.shouldBeBefore(theTime: LocalTime) =
    assertTrue("Expected $this to be before $theTime", this.toLocalTime() < theTime)
