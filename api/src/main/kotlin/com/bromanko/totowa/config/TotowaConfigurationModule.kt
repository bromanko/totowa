package com.bromanko.totowa.config

import com.bromanko.totowa.TotowaConfiguration
import com.bromanko.totowa.core.COMMAND_TIMEOUT_MS
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class TotowaConfigurationModule {
    @Provides
    @Singleton
    @Named(COMMAND_TIMEOUT_MS)
    fun provideCommandTimeoutMs(config: TotowaConfiguration): Long = config.commandTimeoutMs

    @Provides
    @Singleton
    fun provideGoogleConfiguration(config: TotowaConfiguration): GoogleConfiguration = config.google

    @Provides
    @Singleton
    fun provideAuthConfiguration(config: TotowaConfiguration): AuthConfiguration = config.auth

    @Provides
    @Singleton
    fun provideViewStoreConfiguration(config: TotowaConfiguration): ViewStoreConfiguration = config.viewStore
}