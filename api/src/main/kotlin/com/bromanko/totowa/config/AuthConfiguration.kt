package com.bromanko.totowa.config

import com.bromanko.totowa.core.annotation.NoArgConstructor

@NoArgConstructor
data class AuthConfiguration(
    val expirationTimeSeconds: Int = 864_000_000,
    val secret: String
)