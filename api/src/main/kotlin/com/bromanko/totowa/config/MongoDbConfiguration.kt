package com.bromanko.totowa.config

import com.bromanko.totowa.core.annotation.NoArgConstructor

@NoArgConstructor
data class MongoDbConfiguration(val uri: String, val databaseName: String)