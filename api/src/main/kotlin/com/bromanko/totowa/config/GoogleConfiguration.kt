package com.bromanko.totowa.config

import com.bromanko.totowa.core.annotation.NoArgConstructor

@NoArgConstructor
data class GoogleConfiguration(val clientId: String)
