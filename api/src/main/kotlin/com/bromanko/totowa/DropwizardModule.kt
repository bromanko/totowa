package com.bromanko.totowa

import com.bromanko.totowa.config.AuthConfiguration
import com.bromanko.totowa.core.EVENT_STORE
import com.bromanko.totowa.core.VIEW_STORE
import com.bromanko.totowa.core.auth.JsonUnauthorizedHandler
import com.bromanko.totowa.core.auth.JwtAuthenticator
import com.bromanko.totowa.core.auth.UserPrincipal
import com.bromanko.totowa.core.exception.mapper.InvalidTokenExceptionMapper
import com.bromanko.totowa.core.mongo.TotowaMongoTemplate
import com.bromanko.totowa.health.MongoDbHealthCheck
import com.bromanko.totowa.manager.AxonManager
import com.bromanko.totowa.manager.EmbeddedMongoManager
import com.codahale.metrics.health.HealthCheck
import com.github.toastshaman.dropwizard.auth.jwt.JwtAuthFilter
import com.washingtonpost.dropwizard.exceptions.mappers.JsonProcessingExceptionMapper
import com.washingtonpost.dropwizard.exceptions.mappers.RuntimeExceptionMapper
import dagger.Module
import dagger.Provides
import io.dropwizard.auth.AuthDynamicFeature
import io.dropwizard.jersey.errors.EarlyEofExceptionMapper
import io.dropwizard.jersey.errors.IllegalStateExceptionMapper
import io.dropwizard.jersey.optional.EmptyOptionalExceptionMapper
import io.dropwizard.jersey.validation.JerseyViolationExceptionMapper
import io.dropwizard.lifecycle.Managed
import org.jose4j.jwt.consumer.JwtConsumer
import org.jose4j.jwt.consumer.JwtConsumerBuilder
import org.jose4j.keys.HmacKey
import javax.inject.Named
import javax.inject.Singleton
import javax.ws.rs.core.MediaType
import javax.ws.rs.ext.ExceptionMapper

@Module
class DropwizardModule {
    @Provides
    fun provideExceptionMappers(
        jsonProcessingExceptionMapper: JsonProcessingExceptionMapper,
        runtimeExceptionMapper: RuntimeExceptionMapper,
        invalidTokenExceptionMapper: InvalidTokenExceptionMapper
    ) = setOf<ExceptionMapper<out Throwable>>(
        JerseyViolationExceptionMapper(),
        jsonProcessingExceptionMapper,
        EarlyEofExceptionMapper(),
        EmptyOptionalExceptionMapper(),
        IllegalStateExceptionMapper(),
        invalidTokenExceptionMapper,
        runtimeExceptionMapper
    )

    @Provides
    fun providesHealthChecks(
        @Named(EVENT_STORE) eventStoreMongoTemplate: dagger.Lazy<TotowaMongoTemplate>,
        @Named(VIEW_STORE) viewStoreMongoTemplate: dagger.Lazy<TotowaMongoTemplate>
    ): Set<Pair<String, HealthCheck>> {
        return setOf(
            Pair("eventStore", MongoDbHealthCheck(eventStoreMongoTemplate)),
            Pair("viewStore", MongoDbHealthCheck(viewStoreMongoTemplate))
        )
    }

    @Provides
    fun provideJwtConsumer(authConfiguration: AuthConfiguration): JwtConsumer {
        return JwtConsumerBuilder()
            .setAllowedClockSkewInSeconds(30)
            .setRequireExpirationTime()
            .setRequireSubject()
            .setVerificationKey(HmacKey(authConfiguration.secret.toByteArray()))
            .setRelaxVerificationKeyValidation()
            .build()
    }

    @Provides
    fun provideJwtAuthenticator() = JwtAuthenticator()

    @Provides
    fun provideAuthFilter(jwtConsumer: JwtConsumer, jwtAuthenticator: JwtAuthenticator): AuthDynamicFeature {
        return AuthDynamicFeature(
            JwtAuthFilter.Builder<UserPrincipal>()
                .setJwtConsumer(jwtConsumer)
                .setRealm("realm")
                .setPrefix("Bearer")
                .setAuthenticator(jwtAuthenticator)
                .setUnauthorizedHandler(JsonUnauthorizedHandler())
                .buildAuthFilter()
        )
    }

    @Provides
    @Singleton
    fun provideJsonProcessingExceptionMapper(): JsonProcessingExceptionMapper = JsonProcessingExceptionMapper()

    @Provides
    @Singleton
    fun provideRuntimeExceptionMapper(): RuntimeExceptionMapper =
        RuntimeExceptionMapper(false, MediaType.APPLICATION_JSON_TYPE)

    @Provides
    fun provideManagers(
        config: TotowaConfiguration,
        axonManager: AxonManager,
        embeddedMongoManager: EmbeddedMongoManager
    ): Set<Managed> {
        val managed = mutableSetOf<Managed>(axonManager)
        if (config.useInMemoryDb) managed += embeddedMongoManager
        return managed
    }
}