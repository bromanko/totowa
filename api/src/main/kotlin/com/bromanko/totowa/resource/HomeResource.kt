package com.bromanko.totowa.resource

import javax.inject.Inject
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
class HomeResource @Inject constructor() {
    @GET
    fun respond(): Any {
        return object {
            val name = "Totowa"
        }
    }
}