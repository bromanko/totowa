package com.bromanko.totowa.resource

import com.bromanko.totowa.core.annotation.NoArgConstructor
import com.bromanko.totowa.core.auth.UserPrincipal
import com.bromanko.totowa.core.domain.consumer.user.Device
import com.bromanko.totowa.core.domain.consumer.user.Location
import com.bromanko.totowa.core.domain.consumer.user.MoveDeviceCommand
import io.dropwizard.auth.Auth
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.queryhandling.QueryBus
import javax.annotation.security.PermitAll
import javax.inject.Inject
import javax.validation.Valid
import javax.validation.constraints.NotNull
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@NoArgConstructor
data class LocationCreateRequest(
    val deviceIdentifier: String,
    val longitude: Long,
    val latitude: Long,
    val timestamp: Long
)

@Path("/locations")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@PermitAll
class LocationsResource @Inject constructor(
    private val commandGateway: CommandGateway,
    private val queryBus: QueryBus
) {
    @POST
    fun create(@NotNull @Valid request: LocationCreateRequest, @Auth user: UserPrincipal) {
        val command =
            MoveDeviceCommand(
                user.userId,
                Device(request.deviceIdentifier),
                Location(request.latitude, request.longitude, request.timestamp)
            )
        commandGateway.send<Any>(command)
    }

    @GET
    fun list(): List<Location> {
        return list()
    }
}
