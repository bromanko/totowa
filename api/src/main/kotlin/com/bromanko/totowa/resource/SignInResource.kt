package com.bromanko.totowa.resource

import com.bromanko.totowa.core.COMMAND_TIMEOUT_MS
import com.bromanko.totowa.core.annotation.NoArgConstructor
import com.bromanko.totowa.core.auth.AccessToken
import com.bromanko.totowa.core.domain.consumer.user.SignInCommand
import org.axonframework.commandhandling.gateway.CommandGateway
import org.hibernate.validator.constraints.NotEmpty
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named
import javax.validation.Valid
import javax.validation.constraints.NotNull
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@NoArgConstructor
data class SignInRequest(@NotEmpty val idToken: String)

@Path("/signin")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
class SignInResource @Inject constructor(
    @Named(COMMAND_TIMEOUT_MS) private val commandTimeoutMs: Long,
    private val commandGateway: CommandGateway
) {

    @POST
    fun signIn(@NotNull @Valid signInRequest: SignInRequest): AccessToken {
        return commandGateway.sendAndWait<AccessToken>(
            SignInCommand(signInRequest.idToken),
            commandTimeoutMs,
            TimeUnit.MILLISECONDS
        )
    }
}
