package com.bromanko.totowa

import com.bromanko.totowa.core.auth.UserPrincipal
import io.dropwizard.Application
import io.dropwizard.auth.AuthValueFactoryProvider
import io.dropwizard.configuration.EnvironmentVariableSubstitutor
import io.dropwizard.configuration.SubstitutingSourceProvider
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature

class TotowaApplication : Application<TotowaConfiguration>() {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            TotowaApplication().run(*args)
        }
    }

    override fun initialize(bootstrap: Bootstrap<TotowaConfiguration>) {
        bootstrap.configurationSourceProvider =
            SubstitutingSourceProvider(bootstrap.configurationSourceProvider, EnvironmentVariableSubstitutor(false))
    }

    override fun run(config: TotowaConfiguration, env: Environment) {
        val component = DaggerSingletonComponent
            .builder()
            .configuration(config)
            .build()

        registerExceptionMappers(component, env)
        registerHealthChecks(component, env)
        registerResources(component, env)
        registerManagers(component, env)
        registerAuth(component, env)
    }

    private fun registerHealthChecks(
        appComponent: SingletonComponent,
        env: Environment
    ) {
        appComponent.healthChecks
            .forEach { healthCheck -> env.healthChecks().register(healthCheck.first, healthCheck.second) }
    }

    private fun registerManagers(appComponent: SingletonComponent, env: Environment) {
        appComponent.managers.forEach(env.lifecycle()::manage)
    }

    private fun registerResources(appComponent: SingletonComponent, env: Environment) {
        appComponent.resourceLocator.locate().forEach<Any>(env.jersey()::register)
    }

    private fun registerExceptionMappers(appComponent: SingletonComponent, env: Environment) {
        appComponent.exceptionMappers.forEach<Any>(env.jersey()::register)
    }

    private fun registerAuth(appComponent: SingletonComponent, env: Environment) {
        env.jersey().register(appComponent.authFeature)
        env.jersey().register(RolesAllowedDynamicFeature::class.java)
        env.jersey().register(AuthValueFactoryProvider.Binder(UserPrincipal::class.java))
    }
}