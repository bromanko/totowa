package com.bromanko.totowa

import com.bromanko.totowa.config.AuthConfiguration
import com.bromanko.totowa.config.EventStoreConfiguration
import com.bromanko.totowa.config.GoogleConfiguration
import com.bromanko.totowa.config.ViewStoreConfiguration
import com.fasterxml.jackson.annotation.JsonProperty
import io.dropwizard.Configuration
import javax.validation.constraints.NotNull

data class TotowaConfiguration(
    @JsonProperty("useInMemoryDb") val useInMemoryDb: Boolean = false,
    @JsonProperty("commandTimeoutMs") val commandTimeoutMs: Long = 1000 * 30,
    @field:NotNull @JsonProperty("eventStore") val eventStore: EventStoreConfiguration,
    @field:NotNull @JsonProperty("viewStore") val viewStore: ViewStoreConfiguration,
    @field:NotNull @JsonProperty("google") val google: GoogleConfiguration,
    @field:NotNull @JsonProperty("auth") val auth: AuthConfiguration
) : Configuration()
