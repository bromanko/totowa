@file:JvmName("NamedAnnotations")

package com.bromanko.totowa.core

const val EVENT_STORE = "eventStore"
const val VIEW_STORE = "viewStore"
const val COMMAND_TIMEOUT_MS = "commandTimeoutMs"
const val TRACKING_PROCESSORS = "trackingProcessors"
const val EVENT_HANDLERS = "eventHandlers"
const val COMMAND_HANDLERS = "commandHandlers"
const val QUERY_HANDLERS = "queryHandlers"
const val AGGREGATES = "aggregates"