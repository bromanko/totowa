package com.bromanko.totowa.core.mongo

import de.flapdoodle.embed.mongo.MongodProcess
import de.flapdoodle.embed.mongo.MongodStarter
import de.flapdoodle.embed.mongo.config.IMongodConfig
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder
import de.flapdoodle.embed.mongo.config.Net
import de.flapdoodle.embed.mongo.distribution.Version
import de.flapdoodle.embed.process.runtime.Network
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EmbeddedMongo @Inject constructor() {
    private var mongod: MongodProcess? = null
    private val port = Network.getFreeServerPort()

    val uri = "mongodb://127.0.0.1:$port"

    private var config: IMongodConfig = MongodConfigBuilder()
        .version(Version.Main.PRODUCTION)
        .net(Net(port, Network.localhostIsIPv6()))
        .build()

    fun start() {
        if (mongod != null) return

        mongod = MongodStarter.getDefaultInstance().prepare(config).start()
    }

    fun stop() {
        if (mongod == null) return

        mongod!!.stop()
        mongod = null
    }
}
