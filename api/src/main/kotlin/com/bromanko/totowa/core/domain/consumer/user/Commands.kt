package com.bromanko.totowa.core.domain.consumer.user

import org.axonframework.commandhandling.TargetAggregateIdentifier

data class SignInCommand(val idToken: String)

data class CreateUserCommand(
    val userId: UserId,
    val googleUserId: String
)

data class MoveDeviceCommand(@TargetAggregateIdentifier val userId: UserId, val device: Device, val location: Location)
