package com.bromanko.totowa.core.exception.mapper

import com.bromanko.totowa.core.auth.InvalidTokenException
import io.dropwizard.jersey.errors.ErrorMessage
import javax.inject.Inject
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper

class InvalidTokenExceptionMapper @Inject constructor() : ExceptionMapper<InvalidTokenException> {
    override fun toResponse(exception: InvalidTokenException): Response {
        return Response
            .status(Response.Status.BAD_REQUEST)
            .type(MediaType.APPLICATION_JSON_TYPE)
            .entity(ErrorMessage(Response.Status.BAD_REQUEST.statusCode, exception.message))
            .build()
    }
}