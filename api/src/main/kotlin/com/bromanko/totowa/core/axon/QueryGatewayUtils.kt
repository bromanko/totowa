package com.bromanko.totowa.core.axon

import org.axonframework.queryhandling.QueryGateway

inline fun <reified R, Q> QueryGateway.query(query: Q) = this.query(query, R::class.java)