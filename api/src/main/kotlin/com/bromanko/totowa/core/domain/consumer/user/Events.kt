package com.bromanko.totowa.core.domain.consumer.user

import com.bromanko.totowa.core.annotation.NoArgConstructor

@NoArgConstructor
data class UserCreatedEvent(
    val userId: UserId,
    val googleUserId: String
)

@NoArgConstructor
data class UserDeviceAddedEvent(
    val userId: UserId,
    val device: Device
)

@NoArgConstructor
data class UserDeviceMovedEvent(
    val userId: UserId,
    val deviceId: DeviceId,
    val location: Location
)