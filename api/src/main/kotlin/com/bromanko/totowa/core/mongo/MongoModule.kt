package com.bromanko.totowa.core.mongo

import com.bromanko.totowa.TotowaConfiguration
import com.bromanko.totowa.config.MongoDbConfiguration
import com.bromanko.totowa.core.EVENT_STORE
import com.bromanko.totowa.core.VIEW_STORE
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class MongoModule {
    @Provides
    @Named(VIEW_STORE)
    fun provideViewStoreTemplate(config: TotowaConfiguration, embeddedMongo: EmbeddedMongo): TotowaMongoTemplate {
        val mongoConfig = if (config.useInMemoryDb) {
            MongoDbConfiguration(embeddedMongo.uri, config.viewStore.mongoDb.databaseName)
        } else {
            config.viewStore.mongoDb
        }
        return TotowaMongoTemplate(mongoConfig)
    }

    @Provides
    @Named(EVENT_STORE)
    fun provideEventStoreTemplate(config: TotowaConfiguration, embeddedMongo: EmbeddedMongo): TotowaMongoTemplate {
        val mongoConfig = if (config.useInMemoryDb) {
            MongoDbConfiguration(embeddedMongo.uri, config.eventStore.mongoDb.databaseName)
        } else {
            config.viewStore.mongoDb
        }
        return TotowaMongoTemplate(mongoConfig)
    }
}