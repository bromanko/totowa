package com.bromanko.totowa.core.auth

import io.dropwizard.auth.UnauthorizedHandler
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

class JsonUnauthorizedHandler : UnauthorizedHandler {
    private val challengeFormat = "%s realm=\"%s\""

    override fun buildResponse(prefix: String?, realm: String?): Response {
        return Response.status(Response.Status.UNAUTHORIZED)
            .header(
                HttpHeaders.WWW_AUTHENTICATE,
                String.format(challengeFormat, prefix, realm)
            )
            .type(MediaType.APPLICATION_JSON_TYPE)
            .entity(object {
                val code = 401
                val message = "HTTP 401 Unauthorized"
            })
            .build()
    }
}