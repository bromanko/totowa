package com.bromanko.totowa.core.auth

import com.bromanko.totowa.core.axon.query
import com.bromanko.totowa.core.domain.consumer.user.CreateUserCommand
import com.bromanko.totowa.core.domain.consumer.user.FindUserByGoogleUserIdQuery
import com.bromanko.totowa.core.domain.consumer.user.SignInCommand
import com.bromanko.totowa.core.domain.consumer.user.UserId
import com.bromanko.totowa.core.domain.consumer.user.UserSummary
import kotlinx.coroutines.experimental.future.await
import kotlinx.coroutines.experimental.runBlocking
import mu.KotlinLogging
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.queryhandling.QueryGateway
import javax.inject.Inject

class SignInCommandHandler @Inject constructor(
    private val googleTokenVerifier: GoogleTokenVerifier,
    private val accessTokenProvider: AccessTokenProvider,
    private val queryGateway: QueryGateway,
    private val commandGateway: CommandGateway
) {
    companion object {
        val logger = KotlinLogging.logger {}
    }

    @CommandHandler
    fun handle(command: SignInCommand): AccessToken {
        val idToken = googleTokenVerifier.verify(command.idToken)
        var accessToken: AccessToken? = null
        runBlocking {
            val userId = upsertUser(idToken.subject)
            accessToken = accessTokenProvider.getPerishableToken(userId)
        }
        return accessToken!!
    }

    private suspend fun upsertUser(googleUserId: String): String {
        val user =
            queryGateway.query<UserSummary, FindUserByGoogleUserIdQuery>(FindUserByGoogleUserIdQuery(googleUserId))
                .await()
        if (user != null) {
            logger.info { "User with Google Id $googleUserId exists." }
            return user.userId
        }

        logger.info { "Did not find a user with Google Id $googleUserId. Creating one." }
        val userId = UserId.create()
        commandGateway.send<CreateUserCommand>(CreateUserCommand(userId, googleUserId)).await()
        return userId.id
    }
}
