package com.bromanko.totowa.core.auth

import io.dropwizard.auth.Authenticator
import org.jose4j.jwt.MalformedClaimException
import org.jose4j.jwt.consumer.JwtContext
import java.util.Optional

class JwtAuthenticator : Authenticator<JwtContext, UserPrincipal> {

    override fun authenticate(context: JwtContext): Optional<UserPrincipal> {
        return try {
            val subject = context.jwtClaims.subject
            if (subject == null) Optional.empty() else Optional.of(UserPrincipal(subject))
        } catch (e: MalformedClaimException) {
            Optional.empty()
        }
    }
}