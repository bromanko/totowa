package com.bromanko.totowa.core.domain.consumer.user

import org.axonframework.commandhandling.model.AggregateMember
import org.axonframework.eventsourcing.EventSourcingHandler

class Device(val deviceId: DeviceId) {
    @AggregateMember
    val locations: MutableCollection<Location> = mutableListOf()

    @EventSourcingHandler
    fun on(event: UserDeviceMovedEvent) {
        locations.add(event.location)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Device

        return deviceId == other.deviceId
    }

    override fun hashCode(): Int {
        return deviceId.hashCode()
    }
}

typealias DeviceId = String