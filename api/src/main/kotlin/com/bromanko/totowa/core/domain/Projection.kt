package com.bromanko.totowa.core.domain

import com.bromanko.totowa.core.mongo.TotowaMongoTemplate
import dagger.Lazy

abstract class Projection(protected val mongoTemplate: Lazy<TotowaMongoTemplate>) {
    protected inline fun <reified ProjectionType : Any> getCollection() =
        mongoTemplate.get().getCollection<ProjectionType>()
}