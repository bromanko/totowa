package com.bromanko.totowa.core.mongo

import com.bromanko.totowa.config.MongoDbConfiguration
import com.mongodb.MongoClient
import com.mongodb.MongoClientOptions
import com.mongodb.MongoClientURI
import com.mongodb.client.MongoCollection
import org.litote.kmongo.KMongo
import org.litote.kmongo.getCollection

class TotowaMongoTemplate(config: MongoDbConfiguration) {
    private val uri = config.uri
    val databaseName = config.databaseName

    fun createClient(): MongoClient = createClient(null)
    fun createClient(builder: MongoClientOptions.Builder?): MongoClient {
        val uri = if (builder != null)
            MongoClientURI(uri, builder)
        else
            MongoClientURI(uri)
        return KMongo.createClient(uri)
    }

    inline fun <reified TDocument : Any> getCollection(): MongoCollection<TDocument> {
        return createClient().getDatabase(databaseName).getCollection()
    }
}
