package com.bromanko.totowa.core.annotation

@Target(AnnotationTarget.CLASS)
annotation class NoArgConstructor