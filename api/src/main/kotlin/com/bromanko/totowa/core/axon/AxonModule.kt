package com.bromanko.totowa.core.axon

import com.bromanko.totowa.TotowaConfiguration
import com.bromanko.totowa.core.AGGREGATES
import com.bromanko.totowa.core.COMMAND_HANDLERS
import com.bromanko.totowa.core.EVENT_HANDLERS
import com.bromanko.totowa.core.EVENT_STORE
import com.bromanko.totowa.core.QUERY_HANDLERS
import com.bromanko.totowa.core.TRACKING_PROCESSORS
import com.bromanko.totowa.core.auth.SignInCommandHandler
import com.bromanko.totowa.core.domain.consumer.user.UserSummaryProjection
import com.bromanko.totowa.core.mongo.TotowaMongoTemplate
import dagger.Lazy
import dagger.Module
import dagger.Provides
import org.axonframework.commandhandling.CommandBus
import org.axonframework.commandhandling.SimpleCommandBus
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.commandhandling.gateway.DefaultCommandGateway
import org.axonframework.commandhandling.model.AggregateRoot
import org.axonframework.config.Configurer
import org.axonframework.config.DefaultConfigurer
import org.axonframework.config.ProcessingGroup
import org.axonframework.eventhandling.tokenstore.TokenStore
import org.axonframework.eventhandling.tokenstore.inmemory.InMemoryTokenStore
import org.axonframework.eventsourcing.eventstore.EventStorageEngine
import org.axonframework.eventsourcing.eventstore.inmemory.InMemoryEventStorageEngine
import org.axonframework.mongo.DefaultMongoTemplate
import org.axonframework.mongo.eventsourcing.eventstore.MongoEventStorageEngine
import org.axonframework.mongo.eventsourcing.eventstore.documentperevent.DocumentPerEventStorageStrategy
import org.axonframework.mongo.eventsourcing.tokenstore.MongoTokenStore
import org.axonframework.queryhandling.DefaultQueryGateway
import org.axonframework.queryhandling.QueryBus
import org.axonframework.queryhandling.QueryGateway
import org.axonframework.queryhandling.SimpleQueryBus
import org.axonframework.serialization.json.JacksonSerializer
import org.axonframework.serialization.xml.XStreamSerializer
import org.reflections.Reflections
import java.util.Optional
import javax.inject.Named
import javax.inject.Singleton

@Module
class AxonModule {
    @Provides
    @Singleton
    fun provideQueryBus(): QueryBus = SimpleQueryBus()

    @Provides
    @Singleton
    fun provideCommandBus(): CommandBus =
        SimpleCommandBus()

    @Provides
    @Singleton
    fun provideCommandGateway(commandBus: CommandBus): CommandGateway =
        DefaultCommandGateway(commandBus)

    @Provides
    @Named(COMMAND_HANDLERS)
    fun provideCommandHandlers(signInCommandHandler: SignInCommandHandler): Set<@JvmWildcard Any> {
        return setOf(signInCommandHandler)
    }

    @Provides
    @Singleton
    fun provideConfigurer(): Configurer = DefaultConfigurer.defaultConfiguration()

    @Provides
    @Named(EVENT_HANDLERS)
    fun provideEventHandlers(userSummaryProjection: UserSummaryProjection): Set<@JvmWildcard Any> {
        return setOf(
            userSummaryProjection
        )
    }

    @Provides
    @Singleton
    fun provideEventStorageEngine(config: TotowaConfiguration, @Named(EVENT_STORE) mongoTemplate: Lazy<TotowaMongoTemplate>): EventStorageEngine {
        return if (config.useInMemoryDb)
            InMemoryEventStorageEngine() else {
            val templateInstance = mongoTemplate.get()
            val axonTemplate = DefaultMongoTemplate(
                templateInstance.createClient(),
                templateInstance.databaseName
            )
            MongoEventStorageEngine(
                JacksonSerializer(),
                null,
                axonTemplate,
                DocumentPerEventStorageStrategy()
            )
        }
    }

    @Provides
    @Singleton
    fun provideQueryGateway(queryBus: QueryBus): QueryGateway =
        DefaultQueryGateway(queryBus)

    @Provides
    @Singleton
    fun provideTokenStore(config: TotowaConfiguration, @Named(EVENT_STORE) mongoTemplate: Lazy<TotowaMongoTemplate>): TokenStore {
        return if (config.useInMemoryDb)
            InMemoryTokenStore() else {
            val templateInstance = mongoTemplate.get()
            val axonTemplate = DefaultMongoTemplate(
                templateInstance.createClient(),
                templateInstance.databaseName
            )
            MongoTokenStore(
                axonTemplate,
                XStreamSerializer()
            )
        }
    }

    @Provides
    @Named(QUERY_HANDLERS)
    @Singleton
    fun provideQueryHandlers(userSummaryProjection: UserSummaryProjection): Set<@JvmWildcard Any> {
        return setOf(userSummaryProjection)
    }

    @Provides
    @Named(TRACKING_PROCESSORS)
    fun provideTrackingProcessors(): Set<String> = provideTrackingProcessors("com.bromanko.totowa.core.domain")

    private fun provideTrackingProcessors(searchPrefix: String): Set<String> {
        val reflections = Reflections(searchPrefix)
        val trackedProjections = reflections.getTypesAnnotatedWith(TrackedProjection::class.java)

        val processors = mutableSetOf<String>()
        for (tracked in trackedProjections) {
            val processingGroup = tracked.getAnnotation(ProcessingGroup::class.java)
            val name = Optional.ofNullable(processingGroup).map { it.value }.orElse(tracked.name)
            processors += name
        }
        return processors.toSet()
    }

    @Provides
    @Named(AGGREGATES)
    fun provideAggregates(): Set<@JvmWildcard Class<*>> = provideAggregates("com.bromanko.totowa.core.domain")

    private fun provideAggregates(searchPrefix: String): Set<Class<*>> =
        Reflections(searchPrefix).getTypesAnnotatedWith(AggregateRoot::class.java).toSet()
}