package com.bromanko.totowa.core.auth

import com.bromanko.totowa.core.domain.consumer.user.UserId
import java.security.Principal

data class UserPrincipal(private val id: String) : Principal {
    val userId: UserId
        get() = UserId(name)

    override fun getName(): String {
        return id
    }
}