package com.bromanko.totowa.core.domain.consumer.user

class Location(val latitude: Long, val longitude: Long, val timestamp: Long)
