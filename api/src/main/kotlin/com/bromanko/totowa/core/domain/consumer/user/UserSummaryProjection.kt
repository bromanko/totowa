package com.bromanko.totowa.core.domain.consumer.user

import com.bromanko.totowa.core.VIEW_STORE
import com.bromanko.totowa.core.axon.TrackedProjection
import com.bromanko.totowa.core.axon.USER_SUMMARY_PROJECTION
import com.bromanko.totowa.core.domain.Projection
import com.bromanko.totowa.core.mongo.TotowaMongoTemplate
import dagger.Lazy
import org.axonframework.config.ProcessingGroup
import org.axonframework.eventhandling.EventHandler
import org.axonframework.queryhandling.QueryHandler
import org.litote.kmongo.eq
import org.litote.kmongo.findOne
import javax.inject.Inject
import javax.inject.Named

data class UserSummary(val userId: String, val googleUserId: String)

@TrackedProjection
@ProcessingGroup(USER_SUMMARY_PROJECTION)
class UserSummaryProjection @Inject constructor(@Named(VIEW_STORE) mongoTemplate: Lazy<TotowaMongoTemplate>) :
    Projection(mongoTemplate) {

    @EventHandler
    fun on(event: UserCreatedEvent) {
        getCollection<UserSummary>().insertOne(UserSummary(event.userId.id, event.googleUserId))
    }

    @QueryHandler
    fun handle(query: FindUserQuery): UserSummary? {
        return getCollection<UserSummary>().findOne(UserSummary::userId eq query.userId)
    }

    @QueryHandler
    fun handle(query: FindUserByGoogleUserIdQuery): UserSummary? {
        return getCollection<UserSummary>().findOne(UserSummary::googleUserId eq query.googleUserId)
    }
}