package com.bromanko.totowa.core.domain.consumer.user

data class FindUserQuery(val userId: String)
data class FindUserByGoogleUserIdQuery(val googleUserId: String)

data class FindLocations(val userId: String)
