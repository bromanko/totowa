package com.bromanko.totowa.core.auth

import com.bromanko.totowa.config.AuthConfiguration
import mu.KotlinLogging
import org.joda.time.LocalDateTime
import org.jose4j.jws.AlgorithmIdentifiers.HMAC_SHA512
import org.jose4j.jws.JsonWebSignature
import org.jose4j.jwt.JwtClaims
import org.jose4j.jwt.NumericDate
import org.jose4j.keys.HmacKey
import javax.inject.Inject

data class AccessToken(val accessToken: String, val expiresAt: LocalDateTime)

class AccessTokenProvider @Inject constructor(private val authConfig: AuthConfiguration) {
    companion object {
        val logger = KotlinLogging.logger { }
    }

    fun getPerishableToken(userId: String): AccessToken {
        val expiresAt = LocalDateTime.now().plusSeconds(authConfig.expirationTimeSeconds)
        val numericDate = NumericDate.now()
        numericDate.addSeconds(authConfig.expirationTimeSeconds.toLong())

        val claims = JwtClaims()
        claims.subject = userId
        claims.expirationTime = numericDate

        val jws = JsonWebSignature()
        jws.payload = claims.toJson()
        jws.algorithmHeaderValue = HMAC_SHA512
        jws.key = HmacKey(authConfig.secret.toByteArray())

        val accessToken = jws.compactSerialization
        return AccessToken(accessToken, expiresAt)
    }
}