package com.bromanko.totowa.core.auth

import com.bromanko.totowa.config.GoogleConfiguration
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import mu.KotlinLogging
import java.io.IOException
import java.security.GeneralSecurityException
import javax.inject.Inject

class InvalidTokenException(message: String) : RuntimeException(message)

class GoogleTokenVerifier @Inject constructor(private val configuration: GoogleConfiguration) {
    companion object {
        val logger = KotlinLogging.logger {}
    }

    @Throws(GeneralSecurityException::class, IOException::class, InvalidTokenException::class)
    fun verify(idTokenString: String): GoogleIdToken.Payload {
        val transport = NetHttpTransport()
        val jsonFactory = JacksonFactory()

        logger.info { "Google client id: ${configuration.clientId}" }
        val verifier = GoogleIdTokenVerifier.Builder(transport, jsonFactory)
            .setAudience(listOf(configuration.clientId))
            .build()

        var idToken: GoogleIdToken? = null
        try {
            idToken = verifier.verify(idTokenString)
        } catch (ex: Exception) {
            logger.error { "Error verifying idToken" }
        }

        return if (idToken != null)
            idToken.payload else
            throw InvalidTokenException("idToken is invalid")
    }
}