package com.bromanko.totowa.core.domain.consumer.user

import com.bromanko.totowa.core.annotation.NoArgConstructor
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.commandhandling.model.AggregateIdentifier
import org.axonframework.commandhandling.model.AggregateLifecycle.apply
import org.axonframework.commandhandling.model.AggregateMember
import org.axonframework.commandhandling.model.AggregateRoot
import org.axonframework.eventsourcing.EventSourcingHandler
import java.util.UUID

@AggregateRoot
class User() {
    @CommandHandler
    constructor(cmd: CreateUserCommand) : this() {
        apply(UserCreatedEvent(cmd.userId, cmd.googleUserId))
    }

    @AggregateIdentifier
    private var userId: UserId? = null

    @AggregateMember
    val devices: MutableCollection<Device> = mutableListOf()

    @CommandHandler
    fun handle(cmd: MoveDeviceCommand) {
        if (devices.find { it == cmd.device } == null) {
            apply(UserDeviceAddedEvent(this.userId!!, cmd.device))
        }
        apply(
            UserDeviceMovedEvent(
                this.userId!!,
                cmd.device.deviceId,
                cmd.location
            )
        )
    }

    @EventSourcingHandler
    fun on(event: UserCreatedEvent) {
        userId = event.userId
    }

    @EventSourcingHandler
    fun on(event: UserDeviceAddedEvent) {
        devices.add(event.device)
    }

    @EventSourcingHandler
    fun on(event: UserDeviceMovedEvent) {
        val device = devices.find { it.deviceId == event.deviceId }
            ?: throw IllegalStateException("User does not have a device with ID ${event.deviceId}")

        device.locations.add(event.location)
    }
}

@NoArgConstructor
data class UserId(val id: String) {
    companion object {
        fun create() = UserId(UUID.randomUUID().toString())
    }
}