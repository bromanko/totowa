package com.bromanko.totowa

import com.bromanko.totowa.resource.HomeResource
import com.bromanko.totowa.resource.LocationsResource
import com.bromanko.totowa.resource.SignInResource
import dagger.Module
import javax.inject.Inject

@Module
class ResourceLocator @Inject constructor(
    private val homeResource: HomeResource,
    private val signInResource: SignInResource,
    private val locationsResource: LocationsResource
) {
    fun locate() =
        setOf(
            homeResource,
            signInResource,
            locationsResource
        )
}
