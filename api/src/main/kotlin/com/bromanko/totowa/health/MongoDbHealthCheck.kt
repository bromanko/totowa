package com.bromanko.totowa.health

import com.bromanko.totowa.core.mongo.TotowaMongoTemplate
import com.codahale.metrics.health.HealthCheck
import com.mongodb.MongoClientOptions
import dagger.Lazy
import org.bson.BsonDocument
import org.bson.BsonInt32

class MongoDbHealthCheck(private val mongoDbTemplate: Lazy<TotowaMongoTemplate>) : HealthCheck() {
    override fun check(): Result {
        val optionsBuilder = MongoClientOptions.Builder()
        optionsBuilder.serverSelectionTimeout(1000 * 5)
        optionsBuilder.connectTimeout(1000 * 5)
        optionsBuilder.socketTimeout(1000 * 5)
        val client = mongoDbTemplate.get().createClient(optionsBuilder)

        return try {
            val commandDocument = BsonDocument("dbStats", BsonInt32(1)).append("scale", BsonInt32(1))
            client.getDatabase("system").runCommand(commandDocument)
            HealthCheck.Result.healthy()
        } catch (ex: RuntimeException) {
            HealthCheck.Result.unhealthy(ex)
        }
    }
}