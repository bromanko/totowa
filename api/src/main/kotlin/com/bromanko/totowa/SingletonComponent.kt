package com.bromanko.totowa

import com.bromanko.totowa.config.TotowaConfigurationModule
import com.bromanko.totowa.core.axon.AxonModule
import com.bromanko.totowa.core.mongo.MongoModule
import com.codahale.metrics.health.HealthCheck
import dagger.BindsInstance
import dagger.Component
import io.dropwizard.auth.AuthDynamicFeature
import io.dropwizard.lifecycle.Managed
import javax.inject.Singleton
import javax.ws.rs.ext.ExceptionMapper

@Singleton
@Component(
    modules = [
        TotowaConfigurationModule::class,
        DropwizardModule::class,
        MongoModule::class,
        AxonModule::class
    ]
)
interface SingletonComponent {
    val managers: Set<Managed>
    val exceptionMappers: Set<ExceptionMapper<out Throwable>>
    val healthChecks: Set<Pair<String, HealthCheck>>
    val resourceLocator: ResourceLocator
    val authFeature: AuthDynamicFeature

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun configuration(configuration: TotowaConfiguration): Builder

        fun build(): SingletonComponent
    }
}
