package com.bromanko.totowa.manager

import com.bromanko.totowa.core.AGGREGATES
import com.bromanko.totowa.core.COMMAND_HANDLERS
import com.bromanko.totowa.core.EVENT_HANDLERS
import com.bromanko.totowa.core.QUERY_HANDLERS
import com.bromanko.totowa.core.TRACKING_PROCESSORS
import io.dropwizard.lifecycle.Managed
import mu.KLogging
import org.axonframework.commandhandling.CommandBus
import org.axonframework.config.Configuration
import org.axonframework.config.Configurer
import org.axonframework.config.EventHandlingConfiguration
import org.axonframework.eventhandling.tokenstore.TokenStore
import org.axonframework.eventsourcing.eventstore.EventStorageEngine
import org.axonframework.queryhandling.QueryBus
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class AxonManager @Inject constructor(
    configurer: Configurer,
    commandBus: CommandBus,
    queryBus: QueryBus,
    eventStorageEngine: EventStorageEngine,
    tokenStore: TokenStore,
    @Named(QUERY_HANDLERS) queryHandlers: Set<Any>,
    @Named(COMMAND_HANDLERS) commandHandlers: Set<Any>,
    @Named(EVENT_HANDLERS) eventHandlers: Set<Any>,
    @Named(TRACKING_PROCESSORS) trackingProcessors: Set<String>,
    @Named(AGGREGATES) aggregates: Set<Class<*>>
) : Managed {
    companion object : KLogging()

    private val configuration: Configuration

    init {
        configurer.configureCommandBus { commandBus }
        configurer.configureQueryBus { queryBus }
        configurer.configureEmbeddedEventStore { eventStorageEngine }
        commandHandlers.forEach { ch -> configurer.registerCommandHandler { ch } }
        queryHandlers.forEach { qh -> configurer.registerQueryHandler { qh } }

        val ehConfiguration = EventHandlingConfiguration()
        eventHandlers.forEach { eh -> ehConfiguration.registerEventHandler { eh } }
        trackingProcessors.forEach { tp ->
            ehConfiguration.registerTokenStore(tp) { tokenStore }
            ehConfiguration.registerTrackingProcessor(tp)
        }
        configurer.registerModule(ehConfiguration)

        aggregates.forEach { agg -> configurer.configureAggregate(agg) }

        configuration = configurer.buildConfiguration()
    }

    override fun start() {
        configuration.start()
    }

    override fun stop() {
        configuration.shutdown()
    }
}