package com.bromanko.totowa.manager

import com.bromanko.totowa.core.mongo.EmbeddedMongo
import io.dropwizard.lifecycle.Managed
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EmbeddedMongoManager @Inject constructor(private val embeddedMongo: EmbeddedMongo) : Managed {
    override fun start() {
        embeddedMongo.start()
    }

    override fun stop() {
        embeddedMongo.stop()
    }
}