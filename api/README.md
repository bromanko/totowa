# Totowa Api

## Configuration

Configuration uses the [lightbend config](https://github.com/lightbend/config) library.
Values are defined in hierarchical files in the `conf` folder. Any configuration in `application.conf` will be
used first. Values in environment-specific files (ex. `application.prod.conf`) will override these values.

If you need to override a configuration value with a value in the environment you can specify an environment variable:

```
uri: ${MONGODB_URI}
```

It can also optionally be available to override a pre-defined value:

```
basedir = "/whatever/whatever"
basedir = ${?FORCED_BASEDIR}
```

### Secrets

Secrets are handled slightly differently than normal configuration variables. They should always be defined as
environment variables and injected at runtime as Kubernetes secrets. This process is handled via [Bitnami Sealed Secrets](https://github.com/bitnami-labs/sealed-secrets).

#### Adding A Secret

The first step is to create a Kubernetes manifest for a normal secret and export it to a file:

```bash
kubectl create secret generic totowa-api --dry-run --from-literal=foo=bar -o json >mysecret.json
```

Next you need to encrypt this secret for the cluster it is intended to be used in:

```bash
bin/kubeseal --context context-of-cluster --format yaml  <mysecret.json > sealedsecret.yaml
```

This will create a `sealedsecret` yaml file with an encrypted version of the secret data.

Update the `k8s/secrets.yaml` file to include a mapping for the new sealed secret:

```yaml
spec:
  encryptedData:
    mongodb-uri:  {{service.config[env.KUBECTL_CONTEXT].secrets["mongodb-uri"]}}
```

Then place this encrypted value from `sealedsecret.yaml` in the `config/cluster-name/secrets` section of `service.yaml`.
This will inject the secret data for the specific cluster at deployment.