#!/usr/bin/env bash

set -eo pipefail

if [[ -z ${CI+x} ]]; then
    echo "This script is intended to be run in a CI environment"
    exit 1
fi

gcloud auth configure-docker --quiet
