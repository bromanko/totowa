#!/usr/bin/env bash

set -eo pipefail

if [[ -z ${CI+x} ]]; then
    echo "This script is intended to be run in a CI environment"
    exit 1
fi

: ${GCLOUD_PROJECT_NAME?GCLOUD_PROJECT_NAME env variable not set}
: ${GCLOUD_ACCOUNT?GCLOUD_ACCOUNT env variable not set}
: ${GCLOUD_KEYFILE?GCLOUD_KEYFILE env variable not set}

# Store the keyfile in a temp file
tmp_key_file="$(mktemp).json"
trap "rm -f ""$tmp_key_file" 0 2 3 15
echo "$GCLOUD_KEYFILE" > "$tmp_key_file"

gcloud --project "$GCLOUD_PROJECT_NAME" auth activate-service-account \
    "$GCLOUD_ACCOUNT" \
    --key-file="$tmp_key_file"

