#!/usr/bin/env bash

set -euo pipefail

if [[ -z ${CI+x} ]]; then
    echo "This script is intended to be run in a CI environment"
    exit 1
fi

SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"
. ${SCRIPT_PATH}/../../.go/.common

: ${1?GCLOUD_K8S_CLUSTER_NAME must be specified as first argument}
: ${2?GCLOUD_K8S_CLUSTER_ZONE must be specified as second argument}

gcloud container clusters get-credentials $1 --zone $2
