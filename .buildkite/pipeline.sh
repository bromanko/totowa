#!/usr/bin/env bash
set -eo pipefail

#
# This script determines which pipelines to run based on the changed files
# You can define path->pipeline mappings in pipelines.yaml
#

SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"
cd ${SCRIPT_PATH}/../

.buildkite/files-changed.sh | .buildkite/remove-ignored.sh | .buildkite/projects-for-files.sh | .buildkite/merge-pipelines.sh
