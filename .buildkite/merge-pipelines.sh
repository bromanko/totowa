#!/usr/bin/env bash

set -euo pipefail

SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"


CONFIG="$SCRIPT_PATH/pipelines.json"

echo "steps:"
while read -r PROJECT; do
    echo "  - wait"
    echo ""
    [[ -z "$PROJECT" ]] && continue

    PIPELINE=$(cat "$CONFIG" | jq -r ".projects.$PROJECT.pipeline")
    tail -n +2 "$PIPELINE"
done
