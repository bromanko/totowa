#!/usr/bin/env bash

set -euo pipefail

SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"


CONFIG="$SCRIPT_PATH/pipelines.json"
DEPLOY_BRANCH=master
IGNORE=$(cat "$CONFIG" | jq -r ".ignore[]?")

while read -r CHANGED_FILE; do
    [[ -z "$CHANGED_FILE" ]] && continue

    IGNORED=false
    while read -r pattern; do
        if [[ "$CHANGED_FILE" =~ $pattern ]]; then
            IGNORED=true
            break
        fi
    done <<< "$IGNORE"

    if [[ "$IGNORED" = false ]]; then echo -e "$CHANGED_FILE"; fi
done
