#!/usr/bin/env bash

set -euo pipefail

SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"

function beginsWith() {
    if [[ $1 == "" ]]; then
        false
    else
        case $2 in
            "$1"*) true;;
            *) false;;
        esac;
    fi
}

function isProjectAffected {
    local config=$1
    local changed_files=$2
    local project=$3
    local path=$(cat "$config" | jq -r ".projects.$project.path")
    dependencies=$(cat "$config" | jq -r ".projects.$project.dependencies[]?")

    local all_paths=$(printf "%s\n%s" "$path" "$dependencies")
    while read -r path; do
        while read -r changed_file; do
            if [[ "$path" == "." ]] || beginsWith "$path" "$changed_file"; then
                return 0
            fi
        done <<< "$changed_files"
    done <<< "$all_paths"
    return 1
}


CONFIG="$SCRIPT_PATH/pipelines.json"
PROJECTS=$(cat "$CONFIG" | jq -r ".projects | keys[]")

AFFECTED=()
while read -r CHANGED_FILE; do
    while read -r project; do
        if isProjectAffected "$CONFIG" "$CHANGED_FILE" "$project"; then
            AFFECTED+=("$project")
        fi
    done <<< "$PROJECTS"
done

printf "%s\n" "${AFFECTED[@]}" | sort -u
