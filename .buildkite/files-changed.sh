#!/usr/bin/env bash

set -euo pipefail

SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"

function branch {
    if [ -z "${BUILDKITE_BRANCH+x}" ]; then
        git rev-parse --abbrev-ref HEAD
    else
        echo "$BUILDKITE_BRANCH"
    fi
}

CONFIG="$SCRIPT_PATH/pipelines.json"
BRANCH=$(branch)
DEPLOY_BRANCH=master
COMMIT="${BUILDKITE_COMMIT:-$BRANCH}"

if [[ "$BRANCH" == "$DEPLOY_BRANCH" ]]; then
    git --no-pager log -m -1 --name-only --pretty=format: "$COMMIT"
else
    git --no-pager whatchanged --name-only --pretty=format: origin/"$DEPLOY_BRANCH"..HEAD
fi
