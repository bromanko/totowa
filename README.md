# Totowa

A personal data pipeline.

## Build Pipelines

Each monorepo project should define its own build pipeline in Buildkite.
This pipeline can be executed manually or upon pushing whenever
specific files or folders are changed. 

#### Manually Triggered Pipeline

1. Create a new pipeline.yaml file in the [`.buildkite`](./.buildkite) folder.
1. Create a new Pipeline on [Buildkite](https://buildkite.com/bromanko)
    a. Give it a command of `buildkite-agent pipeline upload .buildkite/new-pipeline.yaml`
    a. Give it a label of `:pipeline: Get Pipeline`   

#### Automatically Triggered Pipeline

1. Add a new project entry to [`./buildkite/pipelines.json`](./.buildkite/pipelines.json) 


### Pipelines.json

The [`pipelines.json`](.buildkite/pipelines.json) file contains the configuration for
automatically triggered build pipelines. The files changed in a commit 
are compared with the rules in this file to determine which pipelines to execute.

#### Ignoring Files

The top level `ignore` key contains an array of regex patterns. Any changed file
matching these patterns will be ignored when determining which pipelines to run. 

#### Projects

Projects are defined by adding a key to the `projects` element.

**Path**
Folder that represents the root of the project. Files changed in this path will
trigger the pipeline.

**Pipeline**
The path to the pipeline to be executed when files are changed.

**Dependencies**
Additional paths that may also trigger the pipeline. This is useful to indicate
dependencies between projects.  
